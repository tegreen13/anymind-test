package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"

	"time"

	"bitbucket.org/tegreen13/anymind-test/internal/server"
	"bitbucket.org/tegreen13/anymind-test/internal/storage"
)

const (
	port            = ":80"
	gracefulTimeout = 5 * time.Second
)

func main() {
	log.Printf("application starts on %s port", port)

	db, err := storage.NewDB(os.Getenv("ANYMIND_DB_URL"))
	if err != nil {
		log.Fatalf("unable to connect to database err=%v", err)
	}
	defer db.Close()

	app := server.New(storage.NewStorage(db))
	s := &http.Server{Addr: port, Handler: server.Router(app)}
	go func() {
		err := s.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatalf("can't start server err=%v", err)
		}
	}()

	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	<-stop
	log.Println("got interrupt signal")

	ctx, cancel := context.WithTimeout(context.Background(), gracefulTimeout)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatalf("can't shutdown server err=%v", err)
	}

	log.Println("application successfully stopped")
}
