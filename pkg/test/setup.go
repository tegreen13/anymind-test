package test

import (
	"context"
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"
	"time"

	"bitbucket.org/tegreen13/anymind-test/internal/storage"

	"github.com/jackc/pgx/v4/pgxpool"
)

const pathToSchema = "../../schema.sql"

func hashForSchema() string {
	b := make([]byte, 4)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", b)
}

func SetupDB(t *testing.T) (*pgxpool.Pool, *storage.Storage, func()) {
	var (
		dbURL = os.Getenv("ANYMIND_DB_URL")
		ctx   = context.Background()
	)

	sql, err := ioutil.ReadFile(pathToSchema)
	if err != nil {
		log.Fatal(err)
	}

	db, err := storage.NewDB(dbURL)
	if err != nil {
		log.Fatal(err)
	}

	schemaName := strings.Replace("schema_"+time.Now().Format("20060102_150405"), ".", "_", -1)
	schemaName += "_" + hashForSchema()
	if _, err := db.Exec(ctx, "CREATE SCHEMA "+schemaName); err != nil {
		log.Fatal(err)
	}
	db.Close()

	db, err = storage.NewDB(dbURL + "&search_path=" + schemaName)
	if err != nil {
		log.Fatal(err)
	}

	if _, err := db.Exec(ctx, string(sql)); err != nil {
		log.Fatal(err)
	}

	teardown := func() {
		defer db.Close()
		if t.Failed() {
			fmt.Printf("Test failed: database schema name is %s\n", schemaName)
			return
		}
		if _, err := db.Exec(ctx, "DROP SCHEMA "+schemaName+" CASCADE"); err != nil {
			log.Fatal(err)
		}
	}
	return db, storage.NewStorage(db), teardown
}
