package test

import (
	"strconv"
	"sync"
	"testing"
)

// TestSetupDB checks possibility of parallel execution tests with testing storage
func TestSetupDB(t *testing.T) {
	t.Parallel()
	wg := sync.WaitGroup{}
	for i := 0; i < 5; i++ {
		wg.Add(1)
		go func(str string) {
			t.Run(str, func(t *testing.T) {
				_, _, teardown := SetupDB(t)
				defer teardown()
			})
			wg.Done()
		}(strconv.Itoa(i))
	}
	wg.Wait()
}
