module bitbucket.org/tegreen13/anymind-test

go 1.16

require (
	github.com/jackc/pgx/v4 v4.10.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.10.0 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.5.1
)
