CREATE TABLE events
(
    "id"         BIGSERIAL PRIMARY KEY,
    "amount"     NUMERIC(30, 8)           NOT NULL,
    "datetime"   TIMESTAMP WITH TIME ZONE NOT NULL,
    "created_at" TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);
CREATE INDEX ON events (datetime);

CREATE TABLE balance_by_hours
(
    "id"              BIGSERIAL PRIMARY KEY,
    total_amount      NUMERIC(30, 8)           NOT NULL,
    "amount_per_hour" NUMERIC(30, 8)           NOT NULL,
    "datetime"        TIMESTAMP WITH TIME ZONE NOT NULL
);
CREATE UNIQUE INDEX ON balance_by_hours (datetime);

CREATE FUNCTION aggregate_balance()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS
$$
BEGIN
    INSERT INTO balance_by_hours (datetime, amount_per_hour, total_amount)
    VALUES (DATE_TRUNC('hour', NEW.datetime), NEW.amount,
            COALESCE((select amount_per_hour
                      from balance_by_hours
                      where datetime < NEW.datetime
                      order by datetime desc
                      limit 1), 0) + NEW.amount)
    ON CONFLICT (datetime)
        DO UPDATE SET amount_per_hour = balance_by_hours.amount_per_hour + NEW.amount,
                      total_amount    = balance_by_hours.total_amount + NEW.amount;

    -- To avoid problems with datetime sequential from requests.
    -- But it makes performance very weak in case of random date from requests.
    UPDATE balance_by_hours
    SET total_amount = balance_by_hours.total_amount + NEW.amount
    WHERE datetime > DATE_TRUNC('hour', NEW.datetime);

    RETURN NULL;
END;
$$;

CREATE TRIGGER aggregate_balance
    AFTER INSERT
    ON events
    FOR EACH ROW
EXECUTE PROCEDURE aggregate_balance();
