DB_HOST := localhost
DB_NAME := anymind
DB_USER := admin
DB_PASS := admin
DB_PORT := 54321
DB_URL := "postgresql://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}?sslmode=disable"
DB_SCHEMA := `cat schema.sql`

init-db:
	@docker run -d --name ${DB_NAME} -e POSTGRES_DB=${DB_NAME} -e POSTGRES_USER=${DB_USER} -e POSTGRES_PASSWORD=${DB_PASS} -p ${DB_PORT}:5432 postgres
	# Wait 3 second for container to start
	@sleep 3s

# Better use migration tool for local development
	@docker run -it --rm --network host -e PGPASSWORD=${DB_PASS} postgres psql -h ${DB_HOST} -p ${DB_PORT} -U ${DB_USER} -d ${DB_NAME} -c "${DB_SCHEMA}"

drop-db:
	@docker stop ${DB_NAME}; docker rm ${DB_NAME}

run:
	ANYMIND_DB_URL=${DB_URL} go run ./cmd

test:
	ANYMIND_DB_URL=${DB_URL} go test -race ./...