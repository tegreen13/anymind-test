# Test project for AnyMind 

To run the application first needs start postgres container and than run the service:
```
make init-db
make run 
```

Tests are run with `make test` and also require postgres container.


## API methods:

#### Donate
 
```
POST /wallet/donate

Example: 
    curl -i -X POST http://localhost/wallet/donate -d '{
      "datetime": "2019-10-05T14:48:01+01:00",
      "amount": 1.1
    }'

Responce codes:
    200 - ok
    400 - wrong params
    500 - server error
``` 

#### Get history

```
GET /wallet/history

Example: 
    curl -i http://localhost/wallet/history?start=2011-10-05T10:48:01-00:00&end=2021-10-05T10:48:00-00:00

Responce codes:
    200 - ok
    400 - wrong params
    404 - rocords not found
    500 - server error
``` 
Note: `+` in timezone must be encoded to `%2B`