package server

import (
	"github.com/julienschmidt/httprouter"
)

func Router(s *Server) *httprouter.Router {
	router := httprouter.New()

	router.POST("/wallet/donate", s.Donate)
	router.GET("/wallet/history", s.History)

	return router
}
