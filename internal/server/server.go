package server

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"bitbucket.org/tegreen13/anymind-test/internal/storage"

	"github.com/julienschmidt/httprouter"
	"github.com/shopspring/decimal"
)

type Server struct {
	storage *storage.Storage
}

func New(storage *storage.Storage) *Server {
	return &Server{storage}
}

type donateRequest struct {
	Datetime time.Time       `json:"datetime"` // TODO Not a good practice let user set time of incoming transaction
	Amount   decimal.Decimal `json:"amount"`
}

func (s *Server) Donate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if r.Body == nil {
		log.Printf("Server.Donate body is empty")
		http.Error(w, "body is empty", http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("Server.Donate error reading body err=%v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var data donateRequest
	if err := json.Unmarshal(body, &data); err != nil {
		log.Printf("Server.Donate error unmarshal err=%v", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if data.Amount.LessThanOrEqual(decimal.Zero) {
		http.Error(w, "amount must be greater than 0", http.StatusBadRequest)
		return
	}

	if err := s.storage.Save(r.Context(), data.Datetime, data.Amount); err != nil {
		log.Printf("Server.Donate storage.Save err=%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (s *Server) History(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	q := r.URL.Query()

	start, err := queryParameterTime(q, "start")
	if err != nil {
		log.Printf("Server.History queryParameterTime err=%v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	end, err := queryParameterTime(q, "end")
	if err != nil {
		log.Printf("Server.History queryParameterTime err=%v", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := s.storage.HistoryByHours(r.Context(), start, end)
	if err != nil {
		log.Printf("Server.History storage.HistoryByHours err=%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if len(data) == 0 {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(data); err != nil {
		log.Printf("Server.History encode err=%v", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func queryParameterTime(q url.Values, key string) (time.Time, error) {
	s := q.Get(key)
	if len(s) == 0 {
		return time.Time{}, fmt.Errorf("parameter %s not found", key)
	}

	return time.Parse("2006-01-02T15:04:05-07:00", s)
}
