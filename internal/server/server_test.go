package server

import (
	"bytes"
	"context"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"bitbucket.org/tegreen13/anymind-test/internal/storage"
	"bitbucket.org/tegreen13/anymind-test/pkg/test"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/require"
)

const (
	historyURI = "/wallet/history"
	donateURI  = "/wallet/donate"
)

func TestServer_Donate(t *testing.T) {
	_, s, teardown := test.SetupDB(t)
	defer teardown()

	r := Router(New(s))
	tests := []struct {
		name   string
		body   string
		status int
	}{
		{
			name:   "donate",
			body:   `{"datetime": "2019-10-05T14:48:01+01:00", "amount": 1.1}`,
			status: http.StatusOK,
		},
		{
			name:   "wrong date format",
			body:   `{"datetime": "2019-10-05 14:48:01+01:00", "amount": 1.1}`,
			status: http.StatusBadRequest,
		},
		{
			name:   "negative amount",
			body:   `{"datetime": "2019-10-05T14:48:01+01:00", "amount": -1}`,
			status: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("POST", donateURI, strings.NewReader(tt.body))
			require.NoError(t, err)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)
			require.Equal(t, tt.status, rr.Code)
		})
	}
}

func TestServer_History(t *testing.T) {
	db, s, teardown := test.SetupDB(t)
	defer teardown()

	_, err := db.Exec(context.Background(), `
		insert into events(amount, datetime)
		select random()::NUMERIC(30, 8),'2021-01-01'::timestamp + (random() * '90 days'::interval)
		from generate_Series(1, 100) s;`)
	require.NoError(t, err)

	r := Router(New(s))
	tests := []struct {
		name   string
		params string
		status int
	}{
		{
			name:   "get history",
			params: `?start=2021-01-01T00:00:00-00:00&end=2021-02-01T00:00:00-00:00`,
			status: http.StatusOK,
		},
		{
			name:   "get empty history",
			params: `?start=2011-01-01T00:00:00-00:00&end=2012-01-01T00:00:00-00:00`,
			status: http.StatusNotFound,
		},
		{
			name:   "undecoded url with '+'",
			params: `?start=2011-01-01T00:00:00+00:00&end=2012-01-01T00:00:00+00:00`,
			status: http.StatusBadRequest,
		},
		{
			name:   "wrong date format",
			params: `?start=2021-01-01 00:00:00+00:00&end=2021-02-01 00:00:00+00:00`,
			status: http.StatusBadRequest,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			req, err := http.NewRequest("GET", historyURI+tt.params, nil)
			require.NoError(t, err)
			rr := httptest.NewRecorder()

			r.ServeHTTP(rr, req)
			require.Equal(t, tt.status, rr.Code)
		})
	}
}

func TestServer_IntegrationLogic(t *testing.T) {
	_, s, teardown := test.SetupDB(t)
	defer teardown()

	var (
		r = Router(New(s))

		donate = func(data donateRequest) {
			b, err := json.Marshal(data)
			require.NoError(t, err)

			req, err := http.NewRequest("POST", donateURI, bytes.NewReader(b))
			require.NoError(t, err)

			rr := httptest.NewRecorder()
			r.ServeHTTP(rr, req)
			require.Equal(t, http.StatusOK, rr.Code)
		}
		history = func(params string, expected []storage.HistoryRecord) {
			req, err := http.NewRequest("GET", historyURI+params, nil)
			require.NoError(t, err)

			rr := httptest.NewRecorder()
			r.ServeHTTP(rr, req)
			require.Equal(t, http.StatusOK, rr.Code)

			var resp []storage.HistoryRecord
			err = json.Unmarshal(rr.Body.Bytes(), &resp)
			require.NoError(t, err)
			require.Equal(t, expected, resp)
		}
	)

	for _, d := range []donateRequest{
		{
			Datetime: time.Date(2021, 1, 1, 00, 30, 00, 0, time.UTC),
			Amount:   decimal.NewFromFloat(1),
		},
		{
			Datetime: time.Date(2021, 1, 1, 00, 31, 00, 0, time.UTC),
			Amount:   decimal.NewFromFloat(1.1),
		},
		{
			Datetime: time.Date(2021, 1, 1, 00, 31, 00, 0, time.UTC),
			Amount:   decimal.NewFromFloat(1.2),
		},
		{
			Datetime: time.Date(2021, 1, 1, 01, 31, 00, 0, time.UTC),
			Amount:   decimal.NewFromFloat(0.3),
		},
	} {
		donate(d)
	}

	history(`?start=2021-01-01T00:00:00-00:00&end=2021-01-02T00:00:00-00:00`, []storage.HistoryRecord{
		{
			Datetime: time.Date(2021, 1, 1, 00, 00, 00, 0, time.UTC).In(time.Local),
			Amount:   decimal.NewFromFloat(3.3),
		},
		{
			Datetime: time.Date(2021, 1, 1, 01, 00, 00, 0, time.UTC).In(time.Local),
			Amount:   decimal.NewFromFloat(3.6),
		},
	})

	// test workaround for datetime from the past

	donate(donateRequest{
		Datetime: time.Date(2020, 12, 1, 00, 00, 00, 0, time.UTC),
		Amount:   decimal.NewFromFloat(10.3),
	})

	history(`?start=2020-01-01T00:00:00-00:00&end=2021-01-02T00:00:00-00:00`, []storage.HistoryRecord{
		{
			Datetime: time.Date(2020, 12, 1, 00, 00, 00, 0, time.UTC).In(time.Local),
			Amount:   decimal.NewFromFloat(10.3),
		},
		{
			Datetime: time.Date(2021, 1, 1, 00, 00, 00, 0, time.UTC).In(time.Local),
			Amount:   decimal.NewFromFloat(13.6),
		},
		{
			Datetime: time.Date(2021, 1, 1, 01, 00, 00, 0, time.UTC).In(time.Local),
			Amount:   decimal.NewFromFloat(13.9),
		},
	})
}
