package storage

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

func NewDB(connString string) (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), connString)
}
