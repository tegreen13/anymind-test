package storage

import (
	"context"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/shopspring/decimal"
)

type Storage struct {
	db *pgxpool.Pool
}

func NewStorage(db *pgxpool.Pool) *Storage {
	return &Storage{db}
}

type HistoryRecord struct {
	Datetime time.Time       `json:"datetime"`
	Amount   decimal.Decimal `json:"amount"`
}

func (s *Storage) Save(ctx context.Context, datetime time.Time, amount decimal.Decimal) error {
	_, err := s.db.Exec(ctx, `insert into events(datetime, amount) values ($1, $2);`, datetime, amount)
	return err
}

func (s *Storage) HistoryByHours(ctx context.Context, start time.Time, end time.Time) ([]HistoryRecord, error) {
	rows, err := s.db.Query(ctx, `
		select datetime, total_amount
		from balance_by_hours
		where datetime between $1 and $2
		order by datetime;`, start, end)
	if err != nil {
		return nil, err
	}

	var records []HistoryRecord
	for rows.Next() {
		var hr HistoryRecord
		if err := rows.Scan(&hr.Datetime, &hr.Amount); err != nil {
			return nil, err
		}
		records = append(records, hr)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}

	return records, nil
}
